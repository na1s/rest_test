import json
import unittest

import transaction
from pyramid import testing

from rest_test.models import DBSession, KBItem
from rest_test.views.kb_views import KnowledgeBaseView, KnowledgeItemNotFound


class TestKBItem(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        from sqlalchemy import create_engine
        engine = create_engine('sqlite://')
        from rest_test.models import (
            Base,
            KBItem,
        )
        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        with transaction.manager:
            model = KBItem(title='one', body="test")
            DBSession.add(model)
            DBSession.flush()
            self.new_id = model.id
            model = KBItem(title='two', body="test2")
            DBSession.add(model)
            DBSession.flush()
        self.request = testing.DummyRequest()
        self.view = KnowledgeBaseView(self.request)

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_get_index(self):
        info = self.view.kb_items()
        self.assertEqual(len(info['items']), 2)
        self.assertEqual(info['items'][0].title, 'one')
        self.assertEqual(info['items'][0].body, 'test')
        self.assertEqual(info['items'][1].title, 'two')
        self.assertEqual(info['items'][1].body, 'test2')

    def test_get_index_with_offset_and_limit(self):
        self.request.GET["offset"] = 1
        self.request.GET["limit"] = 1
        info = self.view.kb_items()
        self.assertEqual(len(info['items']), 1)
        self.assertEqual(info['items'][0].title, 'two')
        self.assertEqual(info['items'][0].body, 'test2')

    def test_get_kbitem(self):
        self.request.matchdict['id'] = self.new_id
        info = self.view.kb_item_get()
        self.assertEqual(info.title, 'one')
        self.assertEqual(info.body, 'test')

    def test_get_not_exist_kbitem(self):
        self.request.matchdict['id'] = '11'
        self.assertRaises(KnowledgeItemNotFound, self.view.kb_item_get)

    def test_delete_kbitem(self):
        self.request.matchdict['id'] = self.new_id
        info = self.view.kb_item_delete()
        self.assertEqual(info.title, 'one')
        self.assertEqual(info.body, 'test')
        self.assertRaises(KnowledgeItemNotFound, self.view.kb_item_delete)

    def test_update_not_exist_kbitem(self):
        self.request.json_body = json.loads('{"title":"title","body":"test2"}')
        self.request.matchdict['id'] = '11'
        self.assertRaises(KnowledgeItemNotFound, self.view.kb_item_edit)

    def test_update_kbitem(self):
        self.request.json_body = json.loads('{"title":"title","body":"test2"}')
        self.request.matchdict['id'] = self.new_id
        info = self.view.kb_item_edit()
        self.assertEqual(info.title, 'title')
        self.assertEqual(info.body, 'test2')
        get_request = testing.DummyRequest()
        get_request.matchdict['id'] = self.new_id
        info = self.view.kb_item_get()
        self.assertEqual(info.title, 'title')
        self.assertEqual(info.body, 'test2')

    def test_create_kbitem(self):
        self.request.json_body = json.loads('{"title":"title","body":"test2"}')
        info = self.view.kb_item_create()
        self.assertEqual(info.title, 'title')
        self.assertEqual(info.body, 'test2')
        new_id = info.id
        get_request = testing.DummyRequest()
        get_request.matchdict['id'] = str(new_id)
        view = KnowledgeBaseView(get_request)
        info = view.kb_item_get()
        self.assertEqual(info.title, 'title')
        self.assertEqual(info.body, 'test2')

    def test_search_kbitems_with_title_filter(self):
        self.request.GET["title"] = "one"
        info = self.view.kb_items()
        self.assertEqual(len(info['items']), 1)
        self.assertEqual(info['items'][0].title, 'one')
        self.assertEqual(info['items'][0].body, 'test')

    def test_search_kbitems_with_body_filter(self):
        self.request.GET["body"] = "test2"
        info = self.view.kb_items()
        self.assertEqual(len(info['items']), 1)
        self.assertEqual(info['items'][0].title, 'two')
        self.assertEqual(info['items'][0].body, 'test2')

    def test_search_kbitems_with_title_and_body_filter(self):
        with transaction.manager:
            model = KBItem(title='one', body="test2")
            DBSession.add(model)
        self.request.GET["body"] = "test2"
        self.request.GET["title"] = "one"
        info = self.view.kb_items()
        self.assertEqual(len(info['items']), 1)
        self.assertEqual(info['items'][0].title, 'one')
        self.assertEqual(info['items'][0].body, 'test2')
