# coding: utf8
import unittest

from pyramid import testing


class KnowledgeBaseFunctionalTests(unittest.TestCase):
    def setUp(self):
        from rest_test import main
        from configparser import ConfigParser
        config = ConfigParser()
        config.read('development.ini')
        db_url = config.get('test', 'test_db_connection_string')
        settings = {'sqlalchemy.url': db_url}
        app = main({}, **settings)
        from webtest import TestApp
        self.testapp = TestApp(app)
        from sqlalchemy import create_engine
        from rest_test.models import DBSession, Base
        self.engine = create_engine(db_url)
        Base.metadata.create_all(self.engine)
        DBSession.configure(bind=self.engine)
        return DBSession

    def tearDown(self):
        import transaction
        transaction.abort()
        del self.testapp
        from rest_test.models import DBSession, Base
        Base.metadata.drop_all(self.engine)
        DBSession.remove()
        testing.tearDown()

    def test_empty_items(self):
        res = self.testapp.get('/items', status=200)
        self.assertEqual(b'{"items": []}', res.body)

    def test_create_item(self):
        res = self.testapp.post('/items', status=200, params='{"title": "Статья Дениса","body":"Содержимое статьи"}')
        body = res.json_body
        self.assertEqual(body["title"], "Статья Дениса")
        self.assertEqual(body["body"], "Содержимое статьи")

    def test_create_and_get_item(self):
        res = self.testapp.post('/items', status=200, params='{"title": "Статья Дениса","body":"Содержимое статьи"}')
        body = res.json_body
        self.assertEqual(body["title"], "Статья Дениса")
        self.assertEqual(body["body"], "Содержимое статьи")
        id = body["id"]
        res = self.testapp.get('/items/%d' % id, status=200)
        body = res.json_body
        self.assertEqual(body["title"], "Статья Дениса")
        self.assertEqual(body["body"], "Содержимое статьи")

    def test_get_not_found_item(self):
        res = self.testapp.get('/items/%d' % 1000, status=404)
        body = res.json_body
        self.assertEqual(body["errors"][0], "Knowledge base item doesn't exist")

    def test_edit_validation_error(self):
        res = self.testapp.post('/items', status=200, params='{"title": "Статья Дениса","body":"Содержимое статьи"}')
        body = res.json_body
        self.assertEqual(body["title"], "Статья Дениса")
        self.assertEqual(body["body"], "Содержимое статьи")
        id = body["id"]
        res = self.testapp.put('/items/%d' % id, status=400,
                               params='{"title": "","body":"НеСодержимое статьи"}')
        body = res.json_body
        self.assertEqual(body["errors"][0], "Title shouldn't be blank")

    def test_create_and_edit_item(self):
        res = self.testapp.post('/items', status=200, params='{"title": "Статья Дениса","body":"Содержимое статьи"}')
        body = res.json_body
        self.assertEqual(body["title"], "Статья Дениса")
        self.assertEqual(body["body"], "Содержимое статьи")
        id = body["id"]
        res = self.testapp.put('/items/%d' % id, status=200,
                               params='{"title": "Статья НеДениса","body":"НеСодержимое статьи"}')
        body = res.json_body
        self.assertEqual(body["title"], "Статья НеДениса")
        self.assertEqual(body["body"], "НеСодержимое статьи")

    def test_create_and_delete_item(self):
        res = self.testapp.post('/items', status=200, params='{"title": "Статья Дениса","body":"Содержимое статьи"}')
        body = res.json_body
        self.assertEqual(body["title"], "Статья Дениса")
        self.assertEqual(body["body"], "Содержимое статьи")
        id = body["id"]
        self.testapp.delete('/items/%d' % id, status=200)
        res = self.testapp.get('/items/%d' % id, status=404)
        body = res.json_body
        self.assertEqual(body["errors"][0], "Knowledge base item doesn't exist")

    def test_search_item(self):
        self.testapp.post('/items', status=200, params='{"title": "Статья Дениса","body":"Содержимое статьи"}')
        self.testapp.post('/items', status=200, params='{"title": "Статья НеДениса","body":"НеСодержимое статьи"}')
        self.testapp.post('/items', status=200, params='{"title": "Статья Дениса2","body":"Содержимое статьи"}')
        res = self.testapp.get('/items', status=200)
        body = res.json_body
        self.assertEqual(len(body["items"]), 3)
        res = self.testapp.get('/items', status=200, params={"title": "Дениса"})
        body = res.json_body
        self.assertEqual(len(body["items"]), 3)
        res = self.testapp.get('/items', status=200, params={"title": "НеДениса"})
        body = res.json_body
        self.assertEqual(len(body["items"]), 1)
        res = self.testapp.get('/items', status=200, params={"body": "Содержимое"})
        body = res.json_body
        self.assertEqual(len(body["items"]), 3)
        res = self.testapp.get('/items', status=200, params={"body": "НеСодержимое"})
        body = res.json_body
        self.assertEqual(len(body["items"]), 1)
        res = self.testapp.get('/items', status=200, params={"body": "Содержимое", "title": "Дениса2"})
        body = res.json_body
        self.assertEqual(len(body["items"]), 1)
        res = self.testapp.get('/items', status=200, params={"body": "содержимое"})
        body = res.json_body
        self.assertEqual(len(body["items"]), 3)
