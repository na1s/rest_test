import json

from pyramid.httpexceptions import HTTPBadRequest, HTTPInternalServerError, HTTPNotFound
from pyramid.view import view_config
from pyramid.view import view_defaults
from sqlalchemy.exc import DatabaseError

from rest_test.models import KBItem
from rest_test.models.services.kb_service import KBItemService


class ValidationError(Exception):
    def __init__(self, errors=None, *args, **kwargs):
        super(ValidationError, self).__init__(*args, **kwargs)
        self.errors = errors


class KnowledgeItemNotFound(Exception):
    def __init__(self, errors=None, *args, **kwargs):
        super(KnowledgeItemNotFound, self).__init__(*args, **kwargs)
        self.errors = errors


@view_config(context=DatabaseError)
def failed_sqlalchemy(exception, request):
    error = {'errors': ['Problems with storage, please check logs']}
    return HTTPInternalServerError(
        body=json.dumps(error),
        content_type='application/json')


@view_config(context=ValidationError)
def failed_validation(exception, request):
    error = {'errors': exception.errors}
    return HTTPBadRequest(
        body=json.dumps(error),
        content_type='application/json')


@view_config(context=KnowledgeItemNotFound)
def not_found_item(exception, request):
    error = {'errors': ["Knowledge base item doesn't exist"]}
    return HTTPNotFound(
        body=json.dumps(error),
        content_type='application/json')


@view_defaults(renderer='json', route_name='posts')
class KnowledgeBaseView(object):
    def __init__(self, request):
        self.request = request

    @view_config(route_name='kb_items', renderer='json')
    def kb_items(self):
        offset = int(self.request.params.get('offset', 0))
        limit = int(self.request.params.get('limit', 100))
        title_filter = self.request.params.get('title', None)
        body_filter = self.request.params.get('body', None)
        return {'items': list(KBItemService.all(offset, limit, title_filter, body_filter))}

    @view_config(route_name='kb_item_create', renderer='json')
    def kb_item_create(self):
        entry = KBItem()
        self.update_item_from_params(entry)
        KBItemService.create(entry)
        return entry

    @view_config(route_name='kb_item_edit', renderer='json')
    def kb_item_edit(self):
        item_id = int(self.request.matchdict.get('id', -1))
        entry = self.get_item_or_404(item_id)
        self.update_item_from_params(entry)
        return entry

    @view_config(route_name='kb_item_get', renderer='json')
    def kb_item_get(self):
        item_id = int(self.request.matchdict.get('id', -1))
        entry = self.get_item_or_404(item_id)
        return entry

    @view_config(route_name='kb_item_delete', renderer='json')
    def kb_item_delete(self):
        item_id = int(self.request.matchdict.get('id', -1))
        entry = self.get_item_or_404(item_id)
        KBItemService.delete(entry)
        return entry

    def get_item_or_404(self, item_id):
        entry = KBItemService.by_id(item_id)
        if not entry:
            raise KnowledgeItemNotFound()
        return entry

    def update_item_from_params(self, entry):
        params = self.request.json_body.copy()
        if ("title" not in params) or (not params["title"].strip()):
            raise ValidationError(errors=["Title shouldn't be blank"])
        [params.pop(pk.name, '') for pk in entry.__mapper__.primary_key]
        for param in params:
            if hasattr(entry, param):
                setattr(entry, param, params[param])
