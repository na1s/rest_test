from pyramid.view import view_config


@view_config(route_name='home', renderer='rest_test:templates/home.pt')
def home_view(request):
    return {}
