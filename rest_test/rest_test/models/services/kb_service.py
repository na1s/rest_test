import sqlalchemy as sa

from rest_test import DBSession
from rest_test.models import KBItem


class KBItemService(object):
    @classmethod
    def all(cls, offset, limit, title_filter=None, body_filter=None):
        base_query = DBSession.query(KBItem)
        if title_filter:
            base_query = base_query.filter(KBItem.title.ilike("%%%s%%" % title_filter))
        if body_filter:
            base_query = base_query.filter(KBItem.body.ilike("%%%s%%" % body_filter))
        return base_query.order_by(sa.asc(KBItem.id)).offset(offset).limit(limit)

    @classmethod
    def by_id(cls, id):
        return DBSession.query(KBItem).filter(KBItem.id == id).first()

    @classmethod
    def delete(cls, entry):
        DBSession.delete(entry)

    @classmethod
    def create(cls, entry):
        DBSession.add(entry)
        DBSession.flush()
