from sqlalchemy import (
    Column,
    Index,
    Integer,
    UnicodeText, Unicode)

from .meta import Base


class KBItem(Base):
    __tablename__ = 'kbitems'
    id = Column(Integer, primary_key=True)
    title = Column(Unicode(255), nullable=False)
    body = Column(UnicodeText, default=u'')

    def __json__(self, request):
        return dict(
            id=self.id,
            title=self.title,
            body=self.body
        )


Index('kbitems_index', KBItem.title, mysql_length=255)
