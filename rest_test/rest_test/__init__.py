from pyramid.config import Configurator
from sqlalchemy import engine_from_config

from rest_test.models import DBSession, Base


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    config = Configurator(settings=settings)
    config.include('pyramid_chameleon')
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_static_view('static_app', 'static_app', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('kb_items', '/items', request_method='GET')
    config.add_route('kb_item_create', '/items', request_method='POST')
    config.add_route('kb_item_get', '/items/{id:\d+}', request_method='GET')
    config.add_route('kb_item_edit', '/items/{id:\d+}', request_method='PUT')
    config.add_route('kb_item_delete', '/items/{id:\d+}', request_method='DELETE')
    config.scan()
    return config.make_wsgi_app()
