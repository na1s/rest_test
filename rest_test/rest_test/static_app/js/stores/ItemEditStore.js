var alt = require('../alt');
var ItemEditActions = require('../actions/ItemEditActions');
var ItemSource = require('../sources/ItemSource');

class ItemEditStore {
    constructor() {
        this.errorMessage = null;

        this.bindListeners({
            handleItemFailed: ItemEditActions.ITEM_FAILED,
            handleUpdateItem: ItemEditActions.UPDATE_ITEM,
            handleItemLoading: ItemEditActions.ITEM_LOADING,
            handleItemChanged: ItemEditActions.ITEM_CHANGED,
        });
    }

    handleUpdateItem(item) {
        this.title = item.title;
        this.body = item.body;
        this.errorMessage = null;
        this.isLoading = false;
    }

    handleItemFailed(errorMessage) {
        this.errorMessage = errorMessage;
    }

    handleItemLoading() {
        this.isLoading = true;
    }

    handleItemChanged(item) {
        this.title = item.title;
        this.body = item.body;
    }
}

module.exports = alt.createStore(ItemEditStore, 'ItemEditStore');
