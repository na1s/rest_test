var alt = require('../alt');
var ItemActions = require('../actions/ItemActions');
var ItemSource = require('../sources/ItemSource');

class ItemStore {
    constructor() {
        this.items = [];
        this.titleFilter = "";
        this.bodyFilter = "";
        this.errorMessage = null;

        this.bindListeners({
            handleUpdateItems: ItemActions.UPDATE_ITEMS,
            handleFetchItems: ItemActions.FETCH_ITEMS,
            handleItemsFailed: ItemActions.ITEMS_FAILED,
            handleItemDelete: ItemActions.ITEM_DELETE,
            handleItemsLoading: ItemActions.ITEMS_LOADING,
        });

        this.exportPublicMethods({
            getItem: this.getItem
        });
    }

    handleUpdateItems(items) {
        this.items = items;
        this.errorMessage = null;
        this.isLoading = false;
    }

    handleFetchItems(filter) {
        this.titleFilter = filter.titleFilter;
        this.bodyFilter = filter.bodyFilter;
        this.items = [];
    }

    handleItemsFailed(errorMessage) {
        this.errorMessage = errorMessage;
    }

    handleItemsLoading() {
        this.isLoading = true;
    }

    handleItemDelete(item) {

        this.errorMessage = null;
    }

    getItem(id) {
        var { items } = this.getState();
        for (var i = 0; i < items.length; i += 1) {
            if (items[i].id === id) {
                return items[i];
            }
        }

        return null;
    }
}

module.exports = alt.createStore(ItemStore, 'ItemStore');
