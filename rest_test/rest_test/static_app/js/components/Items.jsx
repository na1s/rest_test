var React = require('react');
var AltContainer = require('alt/AltContainer');
var ItemStore = require('../stores/ItemStore');
var ItemActions = require('../actions/ItemActions');
var Link = require('react-router').Link;
var Items = React.createClass({
    getInitialState() {
        return ItemStore.getState();
    },
    componentDidMount() {
        ItemStore.listen(this.onChange);
        ItemActions.fetchItems("", "");
    },
    componentWillUnmount() {
        ItemStore.unlisten(this.onChange);
    },
    onChange(state) {
        this.setState(state);
    },
    deleteItem(ev) {
        ItemActions.itemDelete(ev.target.getAttribute('data-id'));
    },
    handleTitleFilterChange: function (e) {
        ItemActions.fetchItems(e.target.value, this.state.bodyFilter);
    },
    handleBodyFilterChange: function (e) {
        ItemActions.fetchItems(this.state.titleFilter, e.target.value);
    },
    render() {
        var loading = '';
        if (this.state.isLoading) {
            loading = <div><img src="ajax-loader.gif"/></div>;
        }
        var errorMessage = '';
        if (this.state.errorMessage) {
            errorMessage = <div className="alert alert-danger" role="alert">{this.state.errorMessage}</div>;
        }

        return (
            <div>
                <div className="row items_control">
                    <div className="col-sm-12">
                        {loading}
                        {errorMessage}
                        <form className="form">
                            <div className="form-group">
                                <label className="sr-only" htmlFor="id_title_filter">Title filter</label>
                                <input id="id_title_filter" name="titleFilter" className="form-control" type="text"
                                       value={this.state.titleFilter}
                                       onChange={this.handleTitleFilterChange} placeholder="title filter"/>
                            </div>
                            <div className="form-group">
                                <label className="sr-only" htmlFor="id_body_filter">Body filter</label>
                                <input id="id_body_filter" name="bodyFilter" className="form-control" type="text"
                                       value={this.state.bodyFilter}
                                       onChange={this.handleBodyFilterChange} placeholder="body filter"/>
                            </div>
                            <Link className="btn btn-primary" to={`/kb/`}>Create </Link>
                        </form>


                    </div>
                </div>
                {this.state.items.map((item, i) => {
                    var editButton = (
                        <Link className="btn btn-primary btn-sm" to={`/kb/${item.id}`}>Edit</Link>
                    );
                    var deleteButton = (
                        <button className="btn btn-primary btn-sm" onClick={this.deleteItem} data-id={item.id}>
                            Delete
                        </button>
                    );
                    return (
                        <div className="row item__row" key={i}>
                            <div className="col-sm-12">
                                <div >
                                    <Link className="item__title"
                                          to={`/kb/${item.id}`}>{item.title}</Link>
                                    <div className="item__controls"> { editButton} {deleteButton}</div>
                                    <p>{item.body}</p>

                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>
        );
    }
});

module.exports = Items;
