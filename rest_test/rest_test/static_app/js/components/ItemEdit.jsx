var React = require('react');
var AltContainer = require('alt/AltContainer');
var ItemEditStore = require('../stores/ItemEditStore');
var ItemEditActions = require('../actions/ItemEditActions');

var ItemEdit = React.createClass({
        getInitialState() {
            return {
                title: "",
                body: ""
            };
        },
        componentDidMount() {
            ItemEditStore.listen(this.onChange);
            if (this.props.params.itemId != undefined) {
                ItemEditActions.fetchItem(this.props.params.itemId);
            }
        },
        componentWillUnmount() {
            ItemEditStore.unlisten(this.onChange);
        },
        onChange(state) {
            this.setState(state);
        },
        saveItem() {
            if (this.props.params.itemId != undefined) {
                ItemEditActions.itemEdit(this.props.params.itemId, {title: this.state.title, body: this.state.body});
            }
            else {
                ItemEditActions.itemCreate({title: this.state.title, body: this.state.body});
            }
        },
        handleChange: function (name, e) {
            //var nextState = {};
            //nextState[e.target.name] = e.target.value;
            //this.setState(nextState);
            var item = {
                title: this.state.title,
                body: this.state.body
            };
            item[e.target.name] = e.target.value;
            ItemEditActions.itemChanged(item);
        },
        render() {
            var loading = '';
            if (this.state.isLoading) {
                loading = <div><img src="ajax-loader.gif"/></div>;
            }
            var errorMessage = '';
            if (this.state.errorMessage) {
                errorMessage = <div className="alert alert-danger" role="alert">{this.state.errorMessage}</div>;
            }

            return (
                <div className="row">
                    <div className="col-sm-12">
                        {loading}
                        {errorMessage}
                        <div className="form-group">
                            <label htmlFor="id_title">Title</label>
                            <input id="id_title" name="title" type="text" className="form-control"
                                   placeholder="Knowledge base item title" value={this.state.title}
                                   onChange={this.handleChange.bind(this, 'title')}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="id_body">Body</label>
                            <textarea id="id_body" name="body" rows="20" placeholder="Knowledge base item body"
                                      className="form-control" value={this.state.body}
                                      onChange={this.handleChange.bind(this, 'body')}/>
                        </div>
                        <button className="btn btn-primary" onClick={this.saveItem}>
                            Save
                        </button>
                    </div>
                </div>);

        }


    })
    ;

module.exports = ItemEdit
