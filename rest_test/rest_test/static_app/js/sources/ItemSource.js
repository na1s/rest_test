var ItemActions = require('../actions/ItemActions');
var jQuery = require('jquery');

var ItemSource = {
    fetchItems(titleFilter, bodyFilter) {
        return new Promise(function (resolve, reject) {
            // simulate an asynchronous flow where data is fetched on
            // a remote server somewhere.
            jQuery.ajax({
                url: "/items",
                data: {title: titleFilter, body: bodyFilter},
                success: function (data) {
                    resolve(data.items);
                },
                error: function (response) {
                    reject(response.statusText);
                }
            });
        });

    },
    fetchItem(itemId) {
        return new Promise(function (resolve, reject) {
            // simulate an asynchronous flow where data is fetched on
            // a remote server somewhere.
            jQuery.ajax({
                url: "/items/" + itemId,
                success: function (data) {
                    resolve(data);
                },
                error: function (response) {
                    reject(response.statusText);
                }
            });
        });

    },
    updateItem(itemId, data) {
        return new Promise(function (resolve, reject) {
            // simulate an asynchronous flow where data is fetched on
            // a remote server somewhere.
            jQuery.ajax({
                url: "/items/" + itemId,
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: 'PUT',
                success: function (data) {
                    resolve(data);
                },
                error: function (response) {
                    reject(response.statusText);
                }
            });
        });

    }, createItem(data) {
        return new Promise(function (resolve, reject) {
            // simulate an asynchronous flow where data is fetched on
            // a remote server somewhere.
            jQuery.ajax({
                url: "/items",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: 'POST',
                success: function (data) {
                    resolve(data);
                },
                error: function (response) {
                    reject(response.statusText);
                }
            });
        });

    }
    , deleteItem(itemId) {
        return new Promise(function (resolve, reject) {
            // simulate an asynchronous flow where data is fetched on
            // a remote server somewhere.
            jQuery.ajax({
                url: "/items/" + itemId,
                type: 'DELETE',
                success: function (data) {
                    resolve(data);
                },
                error: function (response) {
                    reject(response.statusText);
                }
            });
        });

    }
};

module.exports = ItemSource;
