var React = require('react');
var ReactDOM = require('react-dom');
var Items = require('./components/Items.jsx');
var ItemEdit = require('./components/ItemEdit.jsx');
var Router = require('react-router').Router;
var Route = require('react-router').Route;
var browserHistory = require('react-router').browserHistory;

ReactDOM.render((
        <Router history={browserHistory}>
            <Route path="/" component={Items}/>
            <Route path="kb/:itemId" component={ItemEdit}/>
            <Route path="kb" component={ItemEdit}/>
        </Router>),
    document.getElementById('ReactApp')
);
