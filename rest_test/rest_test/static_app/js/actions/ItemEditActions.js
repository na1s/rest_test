var alt = require('../alt');
var ItemSource = require('../sources/ItemSource');
var browserHistory = require('react-router').browserHistory;
class ItemEditActions {
    updateItem(item) {
        this.dispatch(item);
    }

    fetchItem(itemId) {
        this.dispatch();
        ItemSource.fetchItem(itemId)
            .then((item) => {
                this.actions.updateItem(item);
            })
            .catch((errorMessage) => {
                this.actions.itemFailed(errorMessage);
            });

    }

    itemLoading() {
        this.dispatch();
    }

    itemFailed(errorMessage) {
        this.dispatch(errorMessage);
    }

    itemChanged(item) {
        this.dispatch(item);
    }

    itemCreate(data) {
        ItemSource.createItem(data)
            .then((item) => {
                browserHistory.push('/')
            })
            .catch((errorMessage) => {
                this.actions.itemFailed(errorMessage);
            });
    }

    itemEdit(itemId, data) {
        ItemSource.updateItem(itemId, data)
            .then((item) => {
                browserHistory.push('/')
            })
            .catch((errorMessage) => {
                this.actions.itemFailed(errorMessage);
            });
    }
}

module.exports = alt.createActions(ItemEditActions);
