var alt = require('../alt');
var ItemSource = require('../sources/ItemSource');
class ItemActions {
    updateItems(items) {
        this.dispatch(items);
    }

    fetchItems(titleFilter, itemFilter) {
        this.dispatch({"titleFilter": titleFilter, "bodyFilter": itemFilter});
        ItemSource.fetchItems(titleFilter, itemFilter)
            .then((items) => {
                this.actions.updateItems(items);
            })
            .catch((errorMessage) => {
                this.actions.itemsFailed(errorMessage);
            });

    }

    itemsLoading() {
        this.dispatch();
    }

    itemsFailed(errorMessage) {
        this.dispatch(errorMessage);
    }


    itemDelete(itemId) {
        this.dispatch(itemId);
        ItemSource.deleteItem(itemId)
            .then((item) => {
                this.actions.fetchItems();
            })
            .catch((errorMessage) => {
                this.actions.itemsFailed(errorMessage);
            });
    }
}

module.exports = alt.createActions(ItemActions);
