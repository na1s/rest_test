rest_test README
==================

Getting Started
---------------

- cd <directory containing this file>

- $VENV/bin/python setup.py develop

- Setup database connection string for development and tests in development.ini

- $VENV/bin/initialize_rest_test_db development.ini

- npm i && npm run build

- $VENV/bin/pserve development.ini

